# 平台介绍

##### 此为.NETFRAMEWORK版本，也是以后更新得主要版本。NETCORE版本见: [https://gitee.com/qikj/QJ_LotusBI](https://gitee.com/qikj/QJ_LotusBI)

OneLotus信息化平台基于.NET架构，集成移动办公、智能办公，支持私有化部署，能够很大程度上节约企业软件开发成本，用户可以直接使用平台已有功能进行信息化建设，框架提供了完整的用户管理，权限管理，表单引擎，流程引擎，报表引擎和信息管理体系，可以大大减化企业信息化建设成本和业务应用开发难度

# 官方网站
开源主页 : [https://gitee.com/qikj/QJ_Single](https://gitee.com/qikj/QJ_Single)

官方网站 : [http://www.qijiekeji.com](http://www.qijiekeji.com/)

演示地址 : [http://www.qijiekeji.com](http://www.qijiekeji.com/)

# 使用必读

1. **此次改的东西较多，基础代码进行了大量重构，已经和之前得版本差别很大了**
1. **重构的重点在于基础框架的完善，因为本人工作的原因，这个框架已经有点类似快速开发框架了，基本告别了原有的OA系统了，但我相信在此框架基础上很容易做出一个比之前还好的Oa系统**
1. **因为做集成项目的原因，采用. net core时会遇到不少不太好解决的坑，所以又用回了. framework 版本，以后的更新也会以这个版本为先，但二者代码差别不大，之后会尽量采取双版本一起更新的机制，希望微软有一天能够大一统**
1. **简单说下3.0版本几个比较重要的升级**
      -  前端基本上全部更换为vue+element ui来构建页面，左侧菜单变为二级菜单模式，右侧工作区采用Tab模式，比之前的版本更清晰了
      -  重构了角色管理模块中角色人员设置，角色权限设置界面，增加了操作权限的管理，增加了角色机构权限的设置，角色可以分三种模式对数据授权，个人，本部门，指定部门
      -  新增菜单时，可以通过增加模板列表页来完成一个包含增删改的通用列表页面，可以自定义一些操作按钮，新增的操作可以在角色授权中进行管理
      -  BI模块可以新建数据表，基本上可以不用再到数据库中去建表了，新增的表可以直接做为数据集供各个模块使用，更新了自定义报表中的表格组件，更加好用美观了
      -  自定义表单新增了一些非常实用的组件，比如树，级连选择器，checkbox勾选框，对之前的一些组件也做了优化
      -  流程引擎更新也很大。比如可以退回上一步，可以定义每一节点页面上元素是否隐藏，以及每一步的操作权限，操作名称，新增了几个自定义的事件，可以配合这些事件来完成不少个性化的功能
# 功能特性

- 一键生成增删改查，批量操作代码
- 支持多种数据库数据库
- 提供了用户，角色，用户组，菜单，日志等常用模块
- 支持数据权限的开发和配置
- 搭配完善的自定义表单以及流程引擎
- 提供BI报表引擎
- 代码全部开源，开发者可以下载源码进行任意，编译成自己的信息化平台
- 平台全功能免费，无任何功能和人数限制。
- 支持私有化部署，下载软件安装包后可以安装在自己的服务器上，数据更安全
- 随时随地办公，平台支持兼容HTML5的浏览器，支持企业微信集成
- 高可扩展性，用户通过简单的学习后，可以自定义配置门户、流程应用、内容管理应用

# 主要功能截图
![image.png](https://cdn.nlark.com/yuque/0/2020/png/456705/1607918165869-df0ad059-6551-4783-9e0c-485515b0b01f.png#align=left&display=inline&height=810&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1080&originWidth=1920&size=464821&status=done&style=shadow&width=1440)
![image.png](https://cdn.nlark.com/yuque/0/2020/png/456705/1607784664796-31da5ae3-23e7-4185-b378-435b889c74b8.png#align=left&display=inline&height=810&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1080&originWidth=1920&size=419081&status=done&style=shadow&width=1440)
![image.png](https://cdn.nlark.com/yuque/0/2020/png/456705/1607689798980-4a8d14e5-d823-431f-90db-9003084428fb.png#align=left&display=inline&height=710&margin=%5Bobject%20Object%5D&originHeight=946&originWidth=1920&size=0&status=done&style=shadow&width=1440)
![image.png](https://cdn.nlark.com/yuque/0/2020/png/456705/1607689798994-354920a3-7d00-4ab6-9e13-3c9b0ddb946a.png#align=left&display=inline&height=710&margin=%5Bobject%20Object%5D&originHeight=946&originWidth=1920&size=0&status=done&style=shadow&width=1440)
![image.png](https://cdn.nlark.com/yuque/0/2020/png/456705/1607918215856-b837d4d6-5dbf-4293-a169-4e2eb23d44f8.png#align=left&display=inline&height=810&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1080&originWidth=1920&size=820366&status=done&style=shadow&width=1440)
![image.png](https://cdn.nlark.com/yuque/0/2020/png/456705/1607689798998-813eece1-589b-4312-b3c2-1c1cc0445a04.png#align=left&display=inline&height=371&margin=%5Bobject%20Object%5D&originHeight=741&originWidth=414&size=0&status=done&style=shadow&width=207) ![image.png](https://cdn.nlark.com/yuque/0/2020/png/456705/1607689799004-b7092e53-3df3-45cf-a512-c6905c0c9099.png#align=left&display=inline&height=382&margin=%5Bobject%20Object%5D&originHeight=763&originWidth=387&size=0&status=done&style=shadow&width=194) ![image.png](https://cdn.nlark.com/yuque/0/2020/png/456705/1607689799005-6862e57e-9667-4ffa-ab60-dccb11a566da.png#align=left&display=inline&height=368&margin=%5Bobject%20Object%5D&originHeight=735&originWidth=416&size=0&status=done&style=none&width=208)
