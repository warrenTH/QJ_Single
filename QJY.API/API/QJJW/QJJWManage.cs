﻿using Newtonsoft.Json.Linq;
using QJY.Data;
using System;
using System.Data;
using System.Linq;

namespace QJY.API
{
    public class QJJWManage
    {


        public class QJ_JWB : BaseEFDaoJW<QJ_JW>
        {

        }

        #region PC端



        public void TBUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtJG = new QJ_JWB().GetDTByCommand("SELECT XBDM AS JGDM,XBMC AS JGMC,IDN FROM jxjd.dbo.xbdm");
            for (int i = 0; i < dtJG.Rows.Count; i++)
            {
                int strJGID = int.Parse(dtJG.Rows[i]["IDN"].ToString());
                string strJGMC = dtJG.Rows[i]["JGMC"].ToString();
                string strJGDM = dtJG.Rows[i]["JGDM"].ToString();

                if (new JH_Auth_BranchB().GetEntities(D => D.RoomCode.ToString() == strJGDM).Count() == 0)
                {
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO [jh_auth_branch] ([ComId], [DeptCode], [DeptName], [DeptShort], [DeptDesc], [DeptRoot], [BranchLevel], [Remark1], [Remark2], [BranchLeader], [WXBMCode], [TXLQX], [RoleQX], [IsHasQX], [RoomCode], [CRUser], [CRDate]) VALUES ('10334', '" + strJGID + "', N'" + strJGMC + "', '2', N'" + strJGMC + "', '1', '0', N'1-" + strJGID + "', N'', N'', '', N'', N'', N'', N'"+ strJGDM + "', N'administrator', '2020-07-26 22:24:01.000');");
                }
            }
            DataTable dtUser = new QJ_JWB().GetDTByCommand("SELECT JS.jsdm,JS.JSMC,JS.XBDM,XB.idn FROM jxjd.dbo.jsdm JS LEFT JOIN jxjd.dbo.xbdm XB ON JS.XBDM=XB.XBDM where XB.idn is not null");

            for (int i = 0; i < dtUser.Rows.Count; i++)
            {
                string jsdm = dtUser.Rows[i]["jsdm"].ToString();
                string strJSXM = dtUser.Rows[i]["JSMC"].ToString();
                int jgdm = int.Parse(dtUser.Rows[i]["idn"].ToString());

                JH_Auth_User user = new JH_Auth_UserB().GetEntity(D => D.UserName == jsdm);
                if (user != null)
                {
                    if (user.BranchCode != jgdm)
                    {
                        user.BranchCode = jgdm;
                        new JH_Auth_UserB().Update(user);
                    }

                }
                else
                {
                    user = new JH_Auth_User();
                    user.BranchCode = jgdm;
                    user.UserName = jsdm;
                    user.UserRealName = strJSXM;
                    user.Sex = "男";
                    user.UserPass = "E99A18C428CB38D5F260853678922E03";
                    user.IsUse = "Y";
                    user.remark = "1";
                    user.ComId = 10334;
                    user.UserOrder = 0;
                    user.CRUser = "administrator";
                    user.CRDate = DateTime.Now;

                    new JH_Auth_UserB().Insert(user);
                }
            }


        }



        public void TBZW(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //职务
            DataTable dtzw = new QJ_JWB().GetDTByCommand("SELECT * FROM jxjd.dbo.DJLB");
            for (int i = 0; i < dtzw.Rows.Count; i++)
            {
                int strJGID = int.Parse(dtzw.Rows[i]["IDN"].ToString());
                string strJGMC = dtzw.Rows[i]["DJMC"].ToString().Trim();
                string strJGDM = dtzw.Rows[i]["QXDJ"].ToString().Trim();

                if (new JH_Auth_RoleB().GetEntities(D => D.WXBQCode.ToString() == strJGDM).Count() == 0)
                {
                    new JH_Auth_RoleB().ExsSclarSql("INSERT INTO [QJBI_JW].[dbo].[jh_auth_role] ([ComId], [RoleName], [RoleDec], [PRoleCode], [DisplayOrder], [leve], [IsUse], [isSysRole], [WXBQCode], [RoleQX], [IsHasQX]) VALUES ('10334', N'"+ strJGMC + "', N'', '0', '0', '0', N'Y', N'N', '"+ strJGDM + "', N'', N'1');");
                }
            }
            DataTable dtUserRole = new QJ_JWB().GetDTByCommand("SELECT * FROM jxjd.dbo.YHDJ");

            for (int i = 0; i < dtUserRole.Rows.Count; i++)
            {
                string jsdm = dtUserRole.Rows[i]["YHDM"].ToString().Trim();
                string strCode = dtUserRole.Rows[i]["QXDJ"].ToString().Trim();
                JH_Auth_Role Role = new JH_Auth_RoleB().GetEntity(D => D.WXBQCode.ToString() == strCode);
                if (new JH_Auth_UserRoleB().GetEntities(D => D.UserName.ToString() == jsdm&&D.RoleCode== Role.RoleCode).Count() == 0)
                {
                    JH_Auth_UserRole UserRole = new JH_Auth_UserRole() { ComId = 0, RoleCode = Role.RoleCode, UserName = jsdm };
                    new JH_Auth_UserRoleB().Insert(UserRole);
                }
            }


        }
        #endregion





    }
}