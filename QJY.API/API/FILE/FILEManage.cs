﻿using Aspose.Words;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace QJY.API
{
    public class FILEManage
    {

        /// <summary>
        /// 按照WORD导出模板导出数据,.NETFRAMEWOK专用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void EXPORTWORD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo, HttpContext contexthttp)
        {
            int pdid = 0;
            int.TryParse(P1, out pdid);

            int piid = 0;
            int.TryParse(P2, out piid);
            Yan_WF_PD PD = new Yan_WF_PDB().GetEntity(d => d.ID == pdid && d.ComId == UserInfo.User.ComId);
            Yan_WF_PI PI = new Yan_WF_PIB().GetEntity(d => d.ID == piid && d.ComId == UserInfo.User.ComId);

            if (PD.ExportFile != null)
            {
                Dictionary<string, string> dictSource = new Dictionary<string, string>();

                List<JH_Auth_ExtendMode> ExtendModes = new List<JH_Auth_ExtendMode>();
                ExtendModes = new JH_Auth_ExtendModeB().GetEntities(D => D.ComId == UserInfo.User.ComId && D.PDID == pdid).ToList();
                foreach (JH_Auth_ExtendMode item in ExtendModes)
                {
                    // string strValue = new JH_Auth_ExtendDataB().GetFiledValue(item.TableFiledColumn, pdid, piid);
                    string strValue = new Yan_WF_PIB().GetFiledValByDC(PI.Content, item.TableFiledColumn);
                    dictSource.Add("qj_" + item.TableFiledColumn, strValue);
                }

                dictSource.Add("qj_CRUser", PI.CRUserName);
                dictSource.Add("qj_BranchName", PI.BranchName);
                dictSource.Add("qj_CRDate", PI.CRDate.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                dictSource.Add("qj_PINUM", PI.ID.ToString());


                List<Yan_WF_TI> tiModels = new Yan_WF_TIB().GetEntities(d => d.PIID == piid).ToList();
                for (int i = 0; i < tiModels.Count; i++)
                {
                    dictSource.Add("qj_Task" + i + ".TaskUser", new JH_Auth_UserB().GetUserRealName(UserInfo.User.ComId.Value, tiModels[i].TaskUserID));
                    dictSource.Add("qj_Task" + i + ".TaskUserView", tiModels[i].TaskUserView ?? "");
                    if (tiModels[i].EndTime != null)
                    {
                        dictSource.Add("qj_Task" + i + ".EndTime", tiModels[i].EndTime.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                }
                var basePath = HttpContext.Current.Request.MapPath("/");

                string filePath = basePath + "/" + PD.ExportFile;
                Aspose.Words.Document doc = new Aspose.Words.Document(filePath);
                //使用文本方式替换
                foreach (string name in dictSource.Keys)
                {
                    doc.Range.Replace(name, dictSource[name], false, false);
                }

                #region 使用书签替换模式


                #endregion
                string Filepath = basePath + "\\Export\\";
                string strFileName = PD.ProcessName + DateTime.Now.ToString("yyMMddHHss") + ".doc";

                doc.Save(Filepath + strFileName, Aspose.Words.Saving.DocSaveOptions.CreateSaveOptions(SaveFormat.Doc));
                msg.Result = strFileName;
                //contexthttp.Response.ContentType = "application/x-zip-compressed";
                //contexthttp.Response.AddHeader("Content-type", "text/html;charset=UTF-8");
                //contexthttp.Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName);
                ////string filename = Server.MapPath("/" + attch.FileUrl);
                //contexthttp.Response.TransmitFile(Filepath + strFileName);
                //contexthttp.Response.End();

            }
        }




    }
}