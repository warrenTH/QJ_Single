﻿using QJY.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Data;
using QJY.Data;
using Newtonsoft.Json;
using QJY.Common;
using Newtonsoft.Json.Linq;

namespace QJY.API
{
    public class APIManage 
    {
      
        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">短信内容</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETUSERJS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            new AuthManage().GETUSERJS(context, msg, P1, P2, UserInfo);
        }

    }
}