﻿using QJY.API;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;

namespace LotusBIWeb
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //lb1.Text = "0x" + StrToHex("{\"ID\":\"002206\"}").Replace(" ", "");

           // lb1.Text = "0x" + StrToHex(TextBox1.Text).Replace(" ", "");


            string strSQL = "SELECT * from Table_dr";
            DataTable dt = new Yan_WF_PDB().GetDTByCommand(strSQL);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strUserName = dt.Rows[i]["a"].ToString();
                string strJGDM = dt.Rows[i]["b"].ToString();

                new Yan_WF_PDB().ExsSclarSql("UPDATE jh_auth_user SET UserLeader='" + strJGDM + "' WHERE UserName='"+ strUserName + "'");



            }

        }


        public string StrToHex(string mStr) //返回处理后的十六进制字符串
        {
            return BitConverter.ToString(
            ASCIIEncoding.Default.GetBytes(mStr)).Replace("-", " ");
        } /* StrToHex */

        protected void Button2_Click(object sender, EventArgs e)
        {
            string ret = string.Empty;
            string paramData = "";
            string posturl = "http://202.195.235.166:8220/mdcs/rest/getDataInfo";
            JObject jb = new JObject();

            jb.Add("page", 1);
            jb.Add("pagesize", 10);
            //jb.Add("paramString", "0x7b224944223a22303032323036227d");
            jb.Add("paramString", lb1.Text);

            string re = PostWebRequest(posturl, JsonConvert.SerializeObject(jb), Encoding.UTF8);
            TextBox2.Text = re;

        }


        public static string PostWebRequest(string postUrl, string paramData, Encoding dataEncode)
        {
            string ret = string.Empty;
            try
            {
                byte[] byteArray = dataEncode.GetBytes(paramData); //转化
                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(new Uri(postUrl));
                webReq.Method = "POST";
                webReq.ContentType = "application/json; charset=UTF-8";
                webReq.Headers.Add("int_num", "b43e6b6351c54f2f87c9e3fc25efa49f");
                webReq.Headers.Add("applyId", "V_TRA_TEACHERS_ALL");
                webReq.Headers.Add("accessToken", "B00EA43933814695E053ABEBC3CA78BC");

                webReq.ContentLength = byteArray.Length;
                Stream newStream = webReq.GetRequestStream();
                newStream.Write(byteArray, 0, byteArray.Length);//写入参数
                newStream.Close();
                HttpWebResponse response = (HttpWebResponse)webReq.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.Default);
                ret = sr.ReadToEnd();
                sr.Close();
                response.Close();
                newStream.Close();
            }
            catch (Exception ex)
            {
                CommonHelp.WriteLOG(ex.ToString());
                return ex.Message;
            }
            return ret;
        }
    }
}