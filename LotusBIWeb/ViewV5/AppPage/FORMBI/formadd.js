﻿var app = new Vue({
    el: '#DATABI_YBZZ',
    components: {

    },
    data: {
        vtype: ComFunJS.getQueryString('vtype', '0'),//0是默认单页模式,1是移动模式,2是弹框模式
        pdid: ComFunJS.getQueryString('pdid', '0'),
        iscopy: ComFunJS.getQueryString('iscopy', 'n'),
        FormCode: "BDGL",
        pddata: {},
        pidata: {},
        poption: { width: '0' },
        FormFile: [],
        loading: true,
        FormDecVisible: false,
        bodyopacity: 95,
        formtatus: "0",
        tabtype: "0",
        isview: true,//是否浏览模式,判断需不需要加载默认值
        nowwidget: {},
        wfdata: [],
        NoValData: [],
        readySize: 0,//组件加载数据
        FormData: {
            wigetitems: [],
            CustomCmponent: {}
        },
        draftdatas: [],
        nowwidget: {
        },
    },
    computed: {
        // 计算属性的 getter
        direction: function () {
            return this.vtype == "0" ? "horizontal" : "horizontal";
        }
    },
    methods: {
        addchildwig() {
            this.readySize++
            // 检查进度是否设置的colSize一致
            if (this.readySize == this.FormData.wigetitems.length) {
                this.layout();
            }
        },
        handleCommand(command) {
            if (command == "qp") {
                this.qp();
            }
            if (command == "zdh") {
                this.zdh();
            }
        },
        getwiget: function (wiggetcode) {
            var datawig = null;
            _.forEach(app.FormData.wigetitems, function (item) {
                if (item.wigdetcode == wiggetcode) {
                    datawig = item;
                }
            })
            return datawig;
        },
        jxfiled: function (nowwidget, jxdata) {
            var js = [];
            if (_.isArray(jxdata)) {
                js = jxdata || JSON.parse(nowwidget.staticdata);

            } else {
                js = JSON.parse(jxdata || nowwidget.staticdata);
            }
            nowwidget.tabfiledlist = [];
            _.forEach(_.keys(js[0]), function (value) {
                var fid = {
                    ColumnName: value,
                    ColumnType: "Str",
                    Dimension: "1",
                    Name: value,
                }
                if (_.findIndex(nowwidget.tabfiledlist, function (o) { return o.ColumnName == fid.ColumnName; }) == -1) {
                    nowwidget.tabfiledlist.push(fid);
                }
            });
        },//解析数据维度
        jxparam: function (val) {
            //解析参数
            var valsql = "";
            if (val) {
                valsql = val;
                var regex2 = /\[(.+?)\]/g;
                var temp = val.match(regex2);
                _.forEach(temp, function (obj) {
                    var tempqval = "";
                    obj = _.trim(obj, '[');
                    obj = _.trim(obj, ']');
                    if (_.startsWith(obj, '&')) {
                        tempqval = ComFunJS.getQueryString(obj.split('&')[1]);
                        valsql = _.replace(valsql, '[' + obj + ']', tempqval);
                    }
                    if (_.startsWith(obj, '@')) {
                        tempqval = ComFunJS.getCookie(obj.split('@')[1]);
                        valsql = _.replace(valsql, '[' + obj + ']', tempqval);
                    }
                    if (_.startsWith(obj, '$')) {
                        var wig = _.find(app.FormData.wigetitems, function (o) {
                            return o.wigdetcode == obj.split('$')[1];
                        })
                        tempqval = wig.value;//获取组件值
                        valsql = _.replace(valsql, '[' + obj + ']', tempqval);
                    }
                })
            }
            return valsql;
        },
        layout: function () {
            app.FormData.wigetitems.forEach(function (wig) {
                if (wig.component == "qjTab") {
                    wig.childpro.Tabs.forEach((tab, index) => {
                        tab.content.forEach((tabitem, indexitem) => {
                            $("#" + wig.wigdetcode).find(".el-tab-pane").eq(index).append($("#" + tabitem.wigdetcode));
                        })
                    })
                }
            })
        },
        zdh: function () {
            $(".containerb").toggleClass("widthmax");
            $(".elmain").toggleClass("pd40");
            $(".elmain").toggleClass("pd0");
        },
        changeop: function (val) {
            $("body").css("opacity", "." + val);
            localStorage.setItem("fopacity", val);
        },
        qp: function () {
            if (document.isFullScreen || document.mozIsFullScreen || document.webkitIsFullScreen) {
                ComFunJS.exitFullscreen()
            } else {
                ComFunJS.requestFullScreen();
            }
        },
        UpdateYBData: function (nowwidget, callback) {
            if (nowwidget.datatype == '2') {//静态数据
                try {
                    if (nowwidget.staticdata) {
                        app.jxfiled(nowwidget, nowwidget.staticdata)
                        var js = JSON.parse(nowwidget.staticdata);
                        var lastdata = js;
                        nowwidget.dataset = lastdata;
                    }
                } catch (e) {
                    app.$notify({
                        title: '失败',
                        message: '错误得JSON格式',
                        type: 'error'
                    });
                }
                return;
            } else {
                //需要先克隆一个对象,不然没法解析参数
                var tempnow = _.cloneDeep(nowwidget);
                if (tempnow.apiurl.indexOf("http:") == -1) {
                    tempnow.apiurl = window.document.location.origin + '/' + tempnow.apiurl;
                }
                if (tempnow.datatype == '3' || tempnow.datatype == '1') {
                    //处理存储过程和API得参数
                    _.forEach(tempnow.proqdata, function (obj) {
                        obj.pvalue = app.jxparam(obj.pvalue);
                    })

                } else {
                    //处理查询参数
                    _.forEach(tempnow.wdlist, function (wd) {
                        if (wd.querydata.length > 0) {
                            _.forEach(wd.querydata, function (wdq) {
                                wdq.glval = app.jxparam(wdq.glval);

                            })
                        }
                    })
                    //处理查询参数
                }

                $.getJSON('/api/Bll/ExeAction?Action=FORMBI_GETYBDATA', { P1: JSON.stringify(tempnow), pagecount: 0, pageNo: 0, pageSize: 0 }, function (resultData) {
                    if (!resultData.ErrorMsg) {
                        nowwidget.dataset = [];
                        if (nowwidget.datatype == "0") {
                            nowwidget.datatotal = resultData.DataLength;
                            nowwidget.dataset = resultData.Result;
                            if (callback) {
                                callback.call(tempdata);
                            }
                        } else {
                            if (resultData.Result) {
                                try {
                                    if (nowwidget.datatype == "1" && nowwidget.apiurl.indexOf("http:") == -1) {
                                        resultData.Result = JSON.parse(resultData.Result).Result;//本地服务处理API
                                    }
                                    app.jxfiled(nowwidget, resultData.Result)
                                    var tempdata = [];
                                    if (_.isArray(resultData.Result)) {
                                        tempdata = resultData.Result;

                                    } else {
                                        tempdata = JSON.parse(resultData.Result);
                                    }
                                    var lastdata = app.datamange(tempdata, nowwidget);
                                    nowwidget.dataset = lastdata;
                                    if (callback) {
                                        callback.call(tempdata);
                                    }
                                } catch (e) {
                                    app.$notify({
                                        title: '失败',
                                        message: '获取数据格式错误',
                                        type: 'error'
                                    });
                                }
                            }
                        }

                    }
                })

            }

        },

        ChangeStatus: function () {
            location.reload();
        },
        GoForm: function () {
            location.href = "/ViewV5/AppPage/FORMBI/FormManage.html?vtype=" + app.vtype + "&piid=" + app.pidata.ID;
        },
        SaveExData: function (dataid) {
            //保存数据利于统计
            $.getJSON("/api/Bll/ExeAction?Action=FORMBI_SAVEEXDATA", { P1: dataid, P2: app.pdid }, function (result) {
                try {
                    let jscode = app.poption.completecode;
                    let func = new Function('piid', jscode);
                    func(app.pidata.ID)
                } catch (e) {

                }
            });
        },
        datachange: function (chidata, wigdetcode) {
            //if (wigdetcode) {
            //    _.forEach(this.FormData.wigetitems, function (item) {
            //        if (item.wigdetcode == wigdetcode) {
            //            item.childpro = JSON.parse(chidata)
            //        }
            //    })
            //}
            //else {
            //    app.nowwidget.childpro = JSON.parse(chidata);
            //}
        },
        StarForm: function () {
            if (app.poption.addcode) {
                try {
                    //添加代码,验证数据可用
                    let jscode = app.poption.addcode;
                    let func = new Function(jscode);
                    var mr = func();
                    if (!mr) {
                        return;
                    }
                } catch (e) {

                }
            }
            this.$confirm('是否要发起表单?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
                center: true
            }).then(() => {
                app.$refs.form.validate(function (boolean, object) {
                    if (app.NoValData.length > 0) {
                        app.$refs.form.clearValidate(app.NoValData);
                    }//遇到不需要验证得,先去除验证
                    if (_.difference(_.keys(object), app.NoValData).length == 0) {
                        app.loading = true;
                        $.getJSON("/api/Bll/ExeAction?Action=FORMBI_STARTWF", { P1: app.FormCode, PDID: app.pdid, csr: "", zsr: "", content: JSON.stringify(app.FormData.wigetitems) }, function (result) {
                            if (!result.ErrorMsg) {
                                app.$notify({
                                    title: '成功',
                                    message: '发起表单成功',
                                    type: 'success'
                                });
                                app.loading = false;
                                app.formtatus = "1";//成功状态
                                app.$refs.form.resetFields();
                                app.pidata = result.Result;
                                if (app.pddata.ProcessType == "-1") {
                                    //没有流程得时候保存数据到扩展表或者关联表里
                                    app.SaveExData(result.Result.ID);
                                }

                                if (app.poption.addendcode) {
                                    try {
                                        let jscode = app.poption.addendcode;
                                        let func = new Function('piid', jscode);
                                        func(app.pidata.ID)
                                    } catch (e) {

                                    }
                                }

                            }
                        });

                    }
                })

            }).catch(() => {
                app.$message({
                    type: 'info',
                    message: '已取消发起操作'
                });
            });
        },
        SaveDraft: function () {

            var draftdata = { "ID": "0", "FormCode": app.FormCode, "FormID": app.pdid, "JsonData": "" };
            //app.FormData.CustomCmponent = this.$refs.CustomCmponent.GetData();
            draftdata.JsonData = JSON.stringify(app.FormData);
            $.getJSON("/api/Bll/ExeAction?Action=FORMBI_SAVEDRAFT", { P1: JSON.stringify(draftdata) }, function (result) {
                if (!result.ErrorMsg) {
                    app.draftdatas.push(result.Result);
                    app.$notify({
                        title: '成功',
                        message: '存草稿成功',
                        type: 'success'
                    });
                }
            })

        },
        DelDraft: function (item, index) {
            $.getJSON("/api/Bll/ExeAction?Action=FORMBI_DELDRAFT", { P1: item.ID }, function (result) {
                if (!result.ErrorMsg) {
                    app.draftdatas.splice(index, 1);

                    app.$notify({
                        title: '成功',
                        message: '删除草稿成功',
                        type: 'success'
                    });
                }
            })
        },
        SelDraft: function (item) {
            app.FormData.wigetitems = [];
            app.FormData = JSON.parse(item.JsonData);
            // this.$refs.CustomCmponent.SelDraft(app.FormData.CustomCmponent);

        },
        GetDraft: function () {
            $.getJSON("/api/Bll/ExeAction?Action=FORMBI_GETDRAFT", { P1: app.FormCode, P2: app.pdid }, function (r) {
                if (!r.ErrorMsg) {
                    app.draftdatas = r.Result;
                }

            })
        },
        InitWF: function () {
            $.getJSON("/api/Bll/ExeAction?Action=FORMBI_GETWFDATA", { P1: app.pdid }, function (result) {
                if (result.ErrorMsg == "") {
                    app.loading = false;
                    app.wfdata = result.Result;
                    app.pddata = result.Result1;
                    app.FormFile = result.Result2;
                    if (app.pddata.Poption) {
                        app.poption = JSON.parse(app.pddata.Poption);
                    }
                    if (app.pddata.Tempcontent) {
                        app.FormData.wigetitems = JSON.parse(app.pddata.Tempcontent);
                        //app.mangewigdet();
                        if (app.iscopy == 'y') {
                            var copydata = JSON.parse(localStorage.getItem("copydata"));
                            app.FormData.wigetitems = copydata.wigetitems;
                        }

                    }
                    if (app.pddata.ProcessType != "-1") {
                        var writefiled = app.wfdata[0].WritableFields.split(',');
                        var hidefiled = app.wfdata[0].ReadableFields.split(',');

                        var intindex = 0;
                        _.forEach(app.FormData.wigetitems, function (wiget) {
                            if (wiget.childpro.hasOwnProperty('disabled')) {
                                wiget.childpro.disabled = _.indexOf(writefiled, wiget.wigdetcode) == -1;
                            }
                            if (wiget.childpro.hasOwnProperty('hide')) {
                                wiget.childpro.hide = _.indexOf(hidefiled, wiget.wigdetcode) > -1;
                            } else {
                                app.$set(wiget.childpro, 'hide', _.indexOf(hidefiled, wiget.wigdetcode) > -1); //添加一个属性
                            }
                            if (wiget.childpro.hide) {
                                 app.NoValData.push("wigetitems." + intindex + ".value")
                            }//隐藏的要去掉验证
                            intindex++;
                        })
                    }

                }
            })
        },
        datamange: function (data, nowwidget) {
            const dv = new DataSet.View().source(data);
            var redata = [];

            //数据过滤
            //获取数据筛选条件
            var qdata = [];
            _.forEach(nowwidget.wdlist, function (wd) {
                if (wd.querydata.length > 0) {
                    _.forEach(wd.querydata, function (wdq) {
                        qdata.push({ colid: wd.colid, cal: wdq.cal, glval: wdq.glval });
                    })
                }
            })
            _.forEach(qdata, function (q) {
                var calval = app.jxparam(q.glval);
                if (calval) {
                    switch (q.cal) {
                        case "0":    //等于
                            {
                                dv.transform({
                                    type: 'filter',
                                    callback(row) { // 判断某一行是否保留，默认返回true
                                        return row[q.colid] == calval;
                                    }
                                });
                            }
                            break;
                        case "1":    //小于
                            {
                                dv.transform({
                                    type: 'filter',
                                    callback(row) { // 判断某一行是否保留，默认返回true
                                        return row[q.colid] < calval;
                                    }
                                });
                            }
                            break;
                        case "2":    //大于
                            {
                                dv.transform({
                                    type: 'filter',
                                    callback(row) { // 判断某一行是否保留，默认返回true
                                        return row[q.colid] > calval;
                                    }
                                });
                            }
                            break;
                        case "3":    //小于
                            {
                                dv.transform({
                                    type: 'filter',
                                    callback(row) { // 判断某一行是否保留，默认返回true
                                        return row[q.colid] != calval;
                                    }
                                });
                            }
                            break;
                        case "4":    //包含
                            {
                                dv.transform({
                                    type: 'filter',
                                    callback(row) { // 判断某一行是否保留，默认返回true
                                        return row[q.colid].indexOf(calval) > -1;
                                    }
                                });
                            }
                            break;
                        default: {

                        }
                    }
                }
            })
            var filds = _.map(nowwidget.wdlist, 'colid');
            if (filds.length > 0) {
                dv.transform({
                    type: 'pick',
                    fields: filds // 决定保留哪些字段，如果不传，则返回所有字段
                });
            }
            redata = dv.rows;
            return redata;
        },

    },
    mounted: function () {
        var pro = this;
        pro.$nextTick(function () {
            var wigdata = ComFunJS.formcomponents;
            for (var i = 0; i < wigdata.length; i++) {
                pro.$options.components[wigdata[i].wigcode] = httpVueLoader(wigdata[i].wigurl);
            }

            if (!ComFunJS.isPC()) {
                pro.vtype = "1";
                $("#FormFoot").addClass("FormFoot");
                //移动端
            }
            pro.InitWF();
            pro.GetDraft();
            if (pro.vtype != "0") {
                pro.zdh();
            }
            if (pro.vtype == "2") {
                $("#FormFoot").addClass("FormFoot");
                //弹框模式,添加底部按钮样式,去除背景
            }

        })
    },
    created() {
        document.body.removeChild(document.getElementById('Loading'))

        var divBJ = document.getElementById('DATABI_YBZZ');
        divBJ.style.display = "block";
    }

})
