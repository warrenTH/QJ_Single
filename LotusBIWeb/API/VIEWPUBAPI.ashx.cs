﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using QJY.API;
using System;
using System.IO;
using System.Web;

namespace LotusBIWeb
{
    /// <summary>
    /// VIEWAPI 的摘要说明
    /// </summary>
    public class VIEWPUBAPI : IHttpHandler
    {
        public string ComId { get; set; }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");
            context.Response.AddHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE"); //支持的http 动作
            context.Response.AddHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type,authorization");
            context.Response.AddHeader("Access-Control-Allow-Credentials", "true");
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";
            string strAction = context.Request["Action"] ?? "";
            string UserName = "";

            string strIP = getIP(context);//用户IP
            Msg_Result Model = new Msg_Result() { Action = strAction.ToUpper(), ErrorMsg = "" };
            if (!string.IsNullOrEmpty(strAction))
            {

                string strCheckString = "";// new CommonHelp().checkconetst(context);
                if (strCheckString != "")
                {
                    Model.ErrorMsg = strAction + "有敏感字符串";
                    new JH_Auth_LogB().InsertLog(strAction, Model.ErrorMsg, strCheckString, UserName, "", 0, strIP);
                }
                else
                {
                    Model.ErrorMsg = "";
                }



            }
            string jsonpcallback = context.Request["jsonpcallback"] ?? "";
            IsoDateTimeConverter timeConverter = new IsoDateTimeConverter();
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            string Result = JsonConvert.SerializeObject(Model, Formatting.Indented, timeConverter).Replace("null", "\"\"");
            if (jsonpcallback != "")
            {
                Result = jsonpcallback + "(" + Result + ")";//支持跨域
            }
            context.Response.Write(Result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        public string getIP(HttpContext context)
        {
            string ipAddr = "";
            try
            {
                HttpRequest Request = context.Request;
                // 如果使用代理，获取真实IP  
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != "")
                    ipAddr = Request.ServerVariables["REMOTE_ADDR"];
                else
                    ipAddr = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipAddr == null || ipAddr == "")
                    ipAddr = Request.UserHostAddress;
                return ipAddr;

            }
            catch (Exception ex)
            {
                return "";
            }
            return ipAddr;
        }
    }



}